//
//  AIAConditionParam.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIAConditionParam.h"

@implementation AIAConditionParam

- (instancetype)initWithName:(NSString*)name{
    if (self = [super initWithImage:@"paramCondicion.png" andName:name]) {
        self.zPosition = kZOrderConditionParam;
        
        [self setColor:[UIColor redColor]];
    }
    return self;
}

@end
