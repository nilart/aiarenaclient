//
//  AIABaseContainerBlock.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class AIAParamBlock;

typedef enum : NSUInteger {
    EBlockAction = 1,
    EBlockCondition = 2,
    EBlockEvent = 3
} EBlockType;

@interface AIABaseBlock : SKSpriteNode

@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat baseHeight;
@property (nonatomic, assign) EBlockType blockType;
@property (nonatomic, assign) BOOL acceptsParams;

- (instancetype)initWithName:(NSString*)name;
- (instancetype)initWithImage:(NSString*)image andName:(NSString*)name;
- (BOOL)canContainBlock:(AIABaseBlock*)block;
- (BOOL)addChildBlock:(AIABaseBlock*)block;
- (void)removeChildBlock:(AIABaseBlock*)block;
- (AIABaseBlock*)getDeepestNodeForLocation:(CGPoint)point;
- (void)updateView;
- (BOOL)addChildParam:(AIAParamBlock*)param;

@end
