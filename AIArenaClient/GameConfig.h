//
//  GameConfig.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#ifndef AIArenaClient_GameConfig_h
#define AIArenaClient_GameConfig_h

#define kZOrderEvent 0.001
#define kZOrderCondition 0.002
#define kZOrderConditionParam 0.003
#define kZOrderAction 0.004
#define kZOrderActionParam 0.005

#define kWorldSize ccs(1500,1500)

#endif
