//
//  AIAAction.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIAAction.h"

@interface AIAAction()

@property (nonatomic, strong) NSMutableArray *params;

@end

@implementation AIAAction

- (instancetype)initWithName:(NSString*)name{
    if (self = [super initWithImage:@"action.png" andName:name]) {
        self.zPosition = kZOrderAction;
        self.blockType = EBlockAction;
        
        self.params = [NSMutableArray new];
        [self setColor:[UIColor orangeColor]];
        self.acceptsParams = YES;
    }
    return self;
}

- (BOOL)addChildParam:(AIAActionParam*)param{
    if ([super addChildParam:param]
        && [param isKindOfClass:[AIAActionParam class]]) {
        [param removeFromParent];
        [self addChild:param];
        // TODO: Calculate for more parms
        param.position = ccpLB(ccp(self.frame.size.width + param.size.width/2 - 17, self.frame.size.height - param.size.height/2), self.frame);
        return YES;
    }
    return NO;
}


- (void)removeFromParent{
    if ([self.parent respondsToSelector:@selector(removeChildBlock:)]) {
        [self.parent performSelector:@selector(removeChildBlock:) withObject:self];
    }
    
    [super removeFromParent];
}

@end
