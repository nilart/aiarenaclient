//
//  AIAAction.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "AIABaseBlock.h"
#import "AIAActionParam.h"

@interface AIAAction : AIABaseBlock

- (BOOL)addChildParam:(AIAActionParam*)param;

@end
