//
//  GameScene.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "GameScene.h"
#import "AIAWorldNode.h"

@interface GameScene()

@property (nonatomic, weak) AIAWorldNode *worldNode;
@property (nonatomic, assign) CGPoint lastTouch;

@end

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    CGSize usableSize = ccs(862, 575);

    AIAWorldNode *worldNode = [[AIAWorldNode alloc]initWithSize:usableSize];
    worldNode.position = ccp((self.frame.size.width - usableSize.width)/2,
                             (self.frame.size.height - usableSize.height)/2);
    [self addChild:worldNode];
    
    SKSpriteNode *leftFrame = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor]
                                                           size:ccs((self.frame.size.width - usableSize.width)/2,
                                                                    self.frame.size.height)];
    
    SKSpriteNode *rightFrame = [leftFrame copy];
    leftFrame.anchorPoint = ccp(0, 0);
    leftFrame.zPosition = Z_INDEX_TOP;
    [self addChild:leftFrame];
    
    rightFrame.anchorPoint = ccp(1, 0);
    rightFrame.position = ccp(self.frame.size.width, 0);
    rightFrame.zPosition = Z_INDEX_TOP;
    [self addChild:rightFrame];
    
    self.worldNode = worldNode;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.worldNode touchesBegan:touches]) {
        
    }else{
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInNode:self];
        self.lastTouch = location;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([self.worldNode touchesMoved:touches]) {
        
    }else{
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInNode:self];
        
        CGFloat diff = location.y - self.lastTouch.y;
        self.worldNode.position = ccp(self.worldNode.position.x, self.worldNode.position.y + diff);
        self.lastTouch = location;
        
        if (self.worldNode.position.y > kWorldSize.height) {
            self.worldNode.position =  ccp(self.position.x, kWorldSize.height) ;
        }else if(self.worldNode.position.y < 0){
            self.worldNode.position = ccp(self.position.x, 0);
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.worldNode touchesEnded:touches];
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
