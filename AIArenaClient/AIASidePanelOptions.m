//
//  AIASidePanelOptions.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 03/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIASidePanelOptions.h"

@interface AIASidePanelOptions()

@property (nonatomic, strong) NSArray *optionsList;

@end

@implementation AIASidePanelOptions

- (instancetype)initWithOptions:(NSArray*)optionsArray{
    if (self = [super init]) {
        _optionsList = optionsArray;
        
    }
    return self;
}

@end

@implementation AIASidePanelObject

+ (instancetype)optionWithType:(ESidePanelObjectType)type{
    return [[[self class] alloc] initWithType:type];
}

- (instancetype)initWithType:(ESidePanelObjectType)type{
    if (self = [super init]) {
        _objectType = type;
    }
    return self;
}

@end
