//
//  AIABaseContainerBlock.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIABaseBlock.h"
#import "AIAParamBlock.h"

@interface AIABaseBlock()

@property (nonatomic, weak) SKSpriteNode *base;

@end

@implementation AIABaseBlock

- (instancetype)initWithImage:(NSString*)image andName:(NSString*)name {
    SKSpriteNode *base = [SKSpriteNode spriteNodeWithImageNamed:image];
    if (self = [super initWithColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.0] size:base.frame.size]) {
        
        SKLabelNode *nameLabel = [SKLabelNode labelNodeWithFontNamed:@"Helvetica-Bold"];
        nameLabel.text = name;
        [nameLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        nameLabel.fontSize = 20;
        nameLabel.position = ccp(cgminx(self.frame) + 25, cgmaxy(self.frame) - 35);
        [self addChild:nameLabel];
        
        // Add base
        [base setCenterRect:CGRectMake(0.0, 0.4, 1.0, 0.2)];
        [base setAnchorPoint:ccp(0.5, 1.0)];
        [base setPosition:ccp(cgmidx(self.frame), cgmaxy(self.frame))];
        [self addChild:base];
        self.base = base;
        
        // Initial values
        _height = self.base.frame.size.height;
        _baseHeight = _height;
        _acceptsParams = NO;
        
    }
    return self;
}

- (void)setColor:(UIColor *)color{
    self.base.color = color;
    self.base.blendMode = SKBlendModeAlpha;
    self.base.colorBlendFactor = 0.8;
}

- (instancetype)initWithName:(NSString*)name{
    NSAssert(NO, @"Must implement this method in the subclass");
    return nil;
}

- (void)setHeight:(CGFloat)height{
    CGFloat scale = (height / _baseHeight);
    [self.base setYScale:scale];
    _height = height;
}

- (BOOL)canContainBlock:(AIABaseBlock*)block{
    if ((long)self.blockType == ((long)block.blockType + 1)) {
        return YES;
    }
    return NO;
}

/// Adds a child to the block
- (BOOL)addChildBlock:(AIABaseBlock*)block{
    // Checks if this block can contain the block
    if ([self canContainBlock:block]) {
        [block removeFromParent];
        [self addChild:block];
        
        return YES;
    }
    return NO;
}

/// Adsd a param type block
- (BOOL)addChildParam:(AIAParamBlock*)param{
    if ([param isKindOfClass:[AIAParamBlock class]]) {

        return YES;
    }
    return NO;
}

- (void)removeChildBlock:(AIABaseBlock*)block{
    NSAssert(NO, @"Must implement this method in the subclass");
}

/// Updates size and shape based on number of children, must be overwritten
- (void)updateView{
    NSAssert(NO, @"Must implement this method in the subclass");
}

/// Returns the deepest node that contains the given point, or self if none
- (AIABaseBlock*)getDeepestNodeForLocation:(CGPoint)point{
    CGPoint location = [self convertPoint:point fromNode:self.parent];
    for (id child in self.children) {
        if ([child isKindOfClass:[AIABaseBlock class]]) {
            AIABaseBlock *childBlock = (AIABaseBlock*)child;
            if (CGRectContainsPoint([childBlock calculateAccumulatedFrame], location)) {
                id deepestNode = [childBlock getDeepestNodeForLocation:location];
                if (deepestNode) return deepestNode; 
            }
        }
    }
    
    // Check if touch actually inside NOT ACCUMULATED frame
    if (CGRectContainsPoint(self.frame, location)) {
        return self;
    }else{
        return nil;
    }
}

@end
