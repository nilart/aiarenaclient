//
//  AIASidePanel.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 03/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIASidePanel.h"

const CGFloat markerSize = 50.0f;

@interface AIASidePanel()

@property (nonatomic, assign) BOOL isMoving;
@property (nonatomic, weak) SKSpriteNode *panelMaker;
@property (nonatomic, assign) ESidePanelStateType panelState;
@property (nonatomic, weak) SKSpriteNode *container;

@end

@implementation AIASidePanel

- (instancetype)initWithSize:(CGSize)size{
    if (self = [super initWithColor:[UIColor whiteColor] size:size]) {
        SKSpriteNode *container = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:ccs(size.width-50, size.height)];
        [self addChild:container];
        self.container = container;
        
        SKSpriteNode *panelMarker = [SKSpriteNode spriteNodeWithColor:[UIColor cyanColor] size:ccs(markerSize, markerSize)];
        panelMarker.position = ccpLB(ccp(self.frame.size.width - panelMarker.frame.size.width / 2,
                                         self.frame.size.height - panelMarker.frame.size.height / 2), self.frame);
        [self addChild:panelMarker];
        self.panelMaker = panelMarker;
        
        self.panelState = ESidePanelClosed;
    }
    return self;
}

- (void)addObjects:(NSArray*)objectsList{
    NSUInteger index = 0;
    const CGFloat margin = 30.0f;
    for (SKSpriteNode *object in objectsList) {
        [object setScale:0.3f]; // Scale down as thumbnail
        object.position = ccpLB(ccp(self.frame.size.width / 2,
                                    self.frame.size.height - margin - (object.frame.size.height * index)), self.frame);
        [self addChild:object];
        index++;
    }
}

- (void)openPanel{
    self.panelState = ESidePanelMoving;
    [self runAction:[SKAction moveBy:CGVectorMake(self.size.width-markerSize, 0) duration:0.5] completion:^{
        self.panelState = ESidePanelOpen;
    }];
}

- (void)closePanel{
    self.panelState = ESidePanelMoving;
    [self runAction:[SKAction moveBy:CGVectorMake(- self.size.width+markerSize, 0) duration:0.5] completion:^{
        self.panelState = ESidePanelClosed;
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"Touch in Side panel");
    UITouch  *touch = (UITouch*)[touches anyObject];
    if (CGRectContainsPoint([self.panelMaker calculateAccumulatedFrame], [touch locationInNode:self])) {
        if (self.panelState == ESidePanelMoving) {
            return;
        }
        
        if (self.panelState == ESidePanelClosed) {
            [self openPanel];
        }else{
            [self closePanel];
        }
    }
}

@end
