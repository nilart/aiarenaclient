//
//  AIASidePanelOptions.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 03/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIASidePanelOptions : NSObject

- (instancetype)initWithOptions:(NSArray*)optionsArray;

@end

typedef NS_ENUM(NSUInteger, ESidePanelObjectType) {
    ESidePanelEvent,
    ESidePanelCondition,
    ESidePAnelAction,
};

@interface AIASidePanelObject : NSObject

@property (nonatomic, assign) ESidePanelObjectType objectType;

- (instancetype)initWithType:(ESidePanelObjectType)type;
+ (instancetype)optionWithType:(ESidePanelObjectType)type;

@end
