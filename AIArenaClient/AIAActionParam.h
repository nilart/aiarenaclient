//
//  AIAActionParam.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "AIAParamBlock.h"

@interface AIAActionParam : AIAParamBlock

@end
