//
//  AIAActionParam.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIAActionParam.h"

@implementation AIAActionParam

- (instancetype)initWithName:(NSString*)name{
    if (self = [super initWithImage:@"paramAccion.png" andName:name]) {
        self.zPosition = kZOrderActionParam;
        
        [self setColor:[UIColor orangeColor]];
    }
    return self;
}

@end
