//
//  AIACondition.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIACondition.h"

@interface AIACondition()

@property (nonatomic, strong) NSMutableArray *actions;
@property (nonatomic, strong) AIAConditionParam *param;

@end

@implementation AIACondition

- (instancetype)initWithName:(NSString*)name{
    if (self = [super initWithImage:@"condicion.png" andName:name]) {
        self.zPosition = kZOrderCondition;
        self.blockType = EBlockCondition;
        
        self.actions = [NSMutableArray new];
        
        [self setColor:[UIColor redColor]];
        [self updateView];
        self.acceptsParams = YES;
    }
    return self;
}

- (void)updateView{
    const CGFloat initialOffset = 100;
    CGFloat totalHeight = 0.0;
    for (AIABaseBlock *action in self.actions) {
        action.position = ccp(108, 0 - totalHeight);
        totalHeight += action.height;
    }
    // Substract offset if content is bigger than it.
    // This way, when resizing it will use already empty space in the block
    if (totalHeight > initialOffset) {
        totalHeight -= initialOffset;
    }
    [self setHeight:self.baseHeight + totalHeight];
    
    if ([self.parent respondsToSelector:@selector(updateView)]) {
        [self.parent performSelector:@selector(updateView)];
    }
}

- (BOOL)addChildBlock:(AIABaseBlock*)block{
    if ([super addChildBlock:block]) {
        [self.actions addObject:block];
        [self updateView];
        return YES;
    }
    return NO;
}

- (void)removeChildBlock:(AIABaseBlock*)block{
    [self.actions removeObject:block];
    [self updateView];
}


- (void)removeFromParent{
    if ([self.parent respondsToSelector:@selector(removeChildBlock:)]) {
        [self.parent performSelector:@selector(removeChildBlock:) withObject:self];
    }
    
    [super removeFromParent];
}

- (BOOL)addChildParam:(AIAConditionParam*)param{
    if ([super addChildParam:param]
        && [param isKindOfClass:[AIAConditionParam class]]) {
        [param removeFromParent];
        [self addChild:param];
        // TODO: Calculate for more parms
        param.position = ccpLB(ccp(self.frame.size.width + param.size.width/2 - 15, self.frame.size.height - param.size.height/2), self.frame);
        return YES;
    }
    return NO;
}

@end
