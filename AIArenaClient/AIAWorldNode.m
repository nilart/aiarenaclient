//
//  AIAWorldNode.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 28/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIAWorldNode.h"
#import "AIAEvent.h"
#import "AIACondition.h"
#import "AIAAction.h"
#import "AIASidePanel.h"

@interface AIAWorldNode()

@property (nonatomic, strong) AIABaseBlock * draggingNode;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableArray *sidePanels;

@end

@implementation AIAWorldNode

- (instancetype)initWithSize:(CGSize)size{
    if (self = [super init]) {
        self.events = [NSMutableArray new];
        self.sidePanels = [NSMutableArray new];
        
        SKSpriteNode *nodeFrame = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:size];
        nodeFrame.anchorPoint = ccp(0, 0);
        nodeFrame.position = ccp(0, 0);
        [self addChild:nodeFrame];
        
        // Create some base objects
//        AIAEvent *event_default = [[AIAEvent alloc] initWithName:@"Event"];
//        event_default.position = ccp(500, 500);
//        [self addChild:event_default];
//        [self.events addObject:event_default];
//        
//        AIAEvent *event_collision = [[AIAEvent alloc] initWithName:@"Event"];
//        event_collision.position = ccp(500, 200);
//        [self addChild:event_collision];
//        [self.events addObject:event_collision];
//        
//        AIACondition *condition = [[AIACondition alloc] initWithName:@"Condition"];
//        condition.position = ccp(150, 200);
//        [self addChild:condition];
//        
//        AIAAction *action1 = [[AIAAction alloc] initWithName:@"Action 1"];
//        action1.position = ccp(150, 350);
//        [self addChild:action1];
//        
//        AIAConditionParam *conditionParam1 = [[AIAConditionParam alloc] initWithName:@"paramCond1"];
//        conditionParam1.position = ccp(150, 450);
//        [self addChild:conditionParam1];
//        
//        AIAActionParam *actionParam1 = [[AIAActionParam alloc]initWithName:@"actionParam1"];
//        actionParam1.position = ccp(150, 550);
//        [self addChild:actionParam1];
        
        AIASidePanel *eventsSidePanel = [[AIASidePanel alloc] initWithSize:ccs(300, size.height)];
        eventsSidePanel.position = ccp(-eventsSidePanel.frame.size.width/2+50, size.height/2);
        [self addChild:eventsSidePanel];
        
        [self.sidePanels addObject:eventsSidePanel];
    }
    return self;
}

- (BOOL)touchesBegan:(NSSet *)touches {
    /* Called when a touch begins */
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    SKNode *targetNode = nil;
    for (SKSpriteNode * node in self.children) {
        if ([node isKindOfClass:[AIABaseBlock class]] &&
            CGRectContainsPoint([node calculateAccumulatedFrame], location)) {
            if (!targetNode) {
                targetNode = node;
            }else{
                SKNode *skNode = (SKNode*)node;
                if (skNode.zPosition > targetNode.zPosition) {
                    targetNode = skNode;
                }
            }
        }
    }
    
    if (targetNode) {
        self.draggingNode = [(AIABaseBlock*)targetNode getDeepestNodeForLocation:location];
        if (self.draggingNode) {
            [self.draggingNode removeFromParent];
            self.draggingNode.position = location;
            [self addChild:self.draggingNode];
            return YES;
        }
    }else{
        for (AIASidePanel *sidePanel in self.sidePanels) {
            [sidePanel touchesBegan:touches withEvent:nil];
        }
    }
    return NO;
}

- (BOOL)touchesMoved:(NSSet *)touches {
    for (UITouch *touch in touches.allObjects) {
        CGPoint location = [touch locationInNode:self];
        
        if (self.draggingNode) {
            [self.draggingNode setPosition:location];
            return YES;
        }
    }
    return NO;
}

- (BOOL)touchesEnded:(NSSet *)touches{
    if (self.draggingNode) {
        UITouch * touch = [touches anyObject];
        CGPoint location = [touch locationInNode:self];
        
        for (SKSpriteNode *node in self.children) {
            if ([node isKindOfClass:[AIABaseBlock class]]
                && node != self.draggingNode) {
                if (CGRectContainsPoint([node calculateAccumulatedFrame], location)) {
                    AIABaseBlock *containerBlock = [(AIABaseBlock*)node getDeepestNodeForLocation:location];
                    if ([self.draggingNode isKindOfClass:[AIAParamBlock class]]) {
                        [containerBlock addChildParam:(AIAParamBlock*)self.draggingNode];
                    }else{
                        [containerBlock addChildBlock:self.draggingNode];
                    }
                    break;
                }
            }
        }
        self.draggingNode = nil;
        
        
        [self checkCollisions];
        
        return YES;
    }
    return NO;
}

- (void)checkCollisions{
    const CGFloat offset = 40.0f;
    for (long i = 0; i<self.events.count; i++) {
        AIABaseBlock *eventBlock = [self.events objectAtIndex:i];
        if (self.events.count > (i+1)) {
            AIABaseBlock *nextEventBlock = [self.events objectAtIndex:i+1];
            CGSize distance = CGSizeDistanceBetweenRects([eventBlock calculateAccumulatedFrame], [nextEventBlock calculateAccumulatedFrame]);
            
            if (distance.height < offset) {
                CGFloat shiftBack = offset - distance.height;
                nextEventBlock.position = ccp(nextEventBlock.position.x, nextEventBlock.position.y - shiftBack);
            }else if(distance.height > offset){
                CGFloat shiftBack = distance.height - offset;
                nextEventBlock.position = ccp(nextEventBlock.position.x, nextEventBlock.position.y + shiftBack);
            }
        }
    }
}

CGSize CGSizeDistanceBetweenRects(CGRect rect1, CGRect rect2)
{
    if (CGRectIntersectsRect(rect1, rect2))
    {
        return ccs(- CGRectIntersection(rect1, rect2).size.width, - CGRectIntersection(rect1, rect2).size.height);
    }
    
    CGRect mostLeft = rect1.origin.x < rect2.origin.x ? rect1 : rect2;
    CGRect mostRight = rect2.origin.x < rect1.origin.x ? rect1 : rect2;
    
    CGFloat xDifference = mostLeft.origin.x == mostRight.origin.x ? 0 : mostRight.origin.x - (mostLeft.origin.x + mostLeft.size.width);
    xDifference = MAX(0, xDifference);
    
    CGRect upper = rect1.origin.y < rect2.origin.y ? rect1 : rect2;
    CGRect lower = rect2.origin.y < rect1.origin.y ? rect1 : rect2;
    
    CGFloat yDifference = upper.origin.y == lower.origin.y ? 0 : lower.origin.y - (upper.origin.y + upper.size.height);
    yDifference = MAX(0, yDifference);
    
    return CGSizeMake(xDifference, yDifference);
}

@end
