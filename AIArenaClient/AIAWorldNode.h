//
//  AIAWorldNode.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 28/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface AIAWorldNode : SKNode

- (instancetype)initWithSize:(CGSize)size;

- (BOOL)touchesBegan:(NSSet *)touches;
- (BOOL)touchesMoved:(NSSet *)touches;
- (BOOL)touchesEnded:(NSSet *)touches;

@end
