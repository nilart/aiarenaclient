//
//  AIASidePanel.h
//  AIArenaClient
//
//  Created by Guillermo Zafra on 03/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, ESidePanelStateType) {
    ESidePanelClosed,
    ESidePanelOpen,
    ESidePanelMoving,
};

@interface AIASidePanel : SKSpriteNode

- (instancetype)initWithSize:(CGSize)size;
- (void)addObjects:(NSArray*)objectsList;
- (void)openPanel;
- (void)closePanel;

@end
