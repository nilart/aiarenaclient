//
//  AIAEvent.m
//  AIArenaClient
//
//  Created by Guillermo Zafra on 27/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AIAEvent.h"

@interface AIAEvent()

@property (nonatomic, strong) NSMutableArray *conditions;

@end

@implementation AIAEvent

- (instancetype)initWithName:(NSString*)name{
    if (self = [super initWithImage:@"event.png" andName:name]) {
        self.zPosition = kZOrderEvent;
        self.blockType = EBlockEvent;
        
        self.conditions = [NSMutableArray new];
        
        [self setColor:[UIColor cyanColor]];
        [self updateView];
    }
    return self;
}

- (void)updateView{
    const CGFloat initialOffset = 150; // Will ignore this height when resizing
    CGFloat totalHeight = 0.0;
    for (AIABaseBlock *condition in self.conditions) {
        condition.position = ccp(45, - 7 - totalHeight);
        totalHeight += condition.height;
    }
    // Substract offset if content is bigger than it.
    // This way, when resizing it will use already empty space in the block
    if (totalHeight > initialOffset) {
        totalHeight -= initialOffset;
    }
    [self setHeight:self.baseHeight + totalHeight];
}

- (BOOL)addChildBlock:(AIABaseBlock*)block{
    if ([super addChildBlock:block]) {
        [self.conditions addObject:block];
        
        [self updateView];
        return YES;
    }
    return NO;
}

- (void)removeChildBlock:(AIABaseBlock*)block{
    [self.conditions removeObject:block];
    [self updateView];
}


@end
